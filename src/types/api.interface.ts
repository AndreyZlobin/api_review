export interface ApiClient {
    get(path: string, params?: object): Promise<any>

    post(path: string, body: object, params?: object,): Promise<any>

    edit(path: string, body: object, params?: object): Promise<any>

    remove(path: string, params?: object): Promise<any>
}
