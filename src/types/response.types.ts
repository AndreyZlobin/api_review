import {Merge} from "./merge";
import {AxiosError, AxiosResponse} from "axios";

export type defaultErrorType = {
    message: string
}

export interface headersInterface {
    "Accept": string
    "Content-Type": string
    Authorization?: string
}

export type successType = {
    success: boolean,
    message?: string
    data?: any
}

export interface SuccessResponse extends Merge<AxiosResponse | any, successType> {
}

export interface ErrorResponse {
    response: AxiosError | Array<defaultErrorType>
    error: boolean
    status: number | boolean
}

export type ResponseType = SuccessResponse | ErrorResponse
