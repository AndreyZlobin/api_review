import axios, {AxiosError, AxiosInstance, AxiosResponse} from "axios";
import {BaseApi} from "./base-api";
import {defaultErrorType, ApiClient, ErrorResponse} from "../../types";

export class AxiosProvider extends BaseApi implements ApiClient {
    private _instance: AxiosInstance;

    constructor(baseUrl: string) {
        super(baseUrl);

        this._instance = axios.create({
            baseURL: this._baseUrl,
        });
    }

    protected _successResponseHandler(response: AxiosResponse) {
        return {...response.data, success: true};
    }

    protected _errorResponseHandler(error: AxiosError) {
        const defaultError: defaultErrorType = {message: "Сервер недоступен"};
        const responseData = error.response?.data?.errors || error.response?.data?.message || [defaultError];
        let errorData: ErrorResponse = {
            error: true,
            response: responseData,
            status: error.response?.status || false
        };
        return {...errorData};
    }

    public async post(path: string, body?: any, params = {}): Promise<any> {
        const url = `${this._baseUrl}/${path}`;

        try {
            const response = await this._instance.post(url, body, params);
            return this._successResponseHandler(response);
        } catch (error) {
            if (error.isAxiosError) {
                return this._errorResponseHandler(error);
            }
            throw new Error(error);
        }
    }

    public async get(path: string, params = {}): Promise<any> {
        try {
            const response = await this._instance.get(path, params);
            return this._successResponseHandler(response);
        } catch (error) {
            if (error.isAxiosError) {
                return this._errorResponseHandler(error);
            }
            throw new Error(error);
        }
    }

    public async patch(path: string, body = {}, params = {}): Promise<any> {
        try {
            const response = await this._instance.patch(path, body, params);
            return this._successResponseHandler(response);
        } catch (error) {
            if (error.isAxiosError) {
                return this._errorResponseHandler(error);
            }
            throw new Error(error);
        }
    }

    public async remove(path: string, params = {}): Promise<any> {
        try {
            const response = await this._instance.delete(path, params);
            return this._successResponseHandler(response);
        } catch (error) {
            if (error.isAxiosError) {
                return this._errorResponseHandler(error);
            }
            throw new Error(error);
        }
    }
}
