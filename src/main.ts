import {$http} from "./http";
import {brandEndpoints} from "./endpoints";

const app = document.querySelector<HTMLDivElement>('#app')!
const test1 = document.querySelector<HTMLElement>('.test-1')!
const test2 = document.querySelector<HTMLElement>('.test-2')!
import './style.css'

app.innerHTML = `
  <h1>Api</h1>
  <a href="https://tech-online.herokuapp.com/api-docs/">Test api: see more</a>
`
const submitData = {
    name: 'foo',
    country: 'bar',
}

// test incorrect data for post request
const incorrectData = {
    foo: 'foo',
    bar: 'bar',
}
console.log({incorrectData});

const {getBrands, createBrand} = brandEndpoints

const getRequest = async () => {
    const result = await $http.get(getBrands, {params: {answer: 42}})
    test1.innerHTML = `${JSON.stringify(result, null, 2)}`
};
getRequest();

const postRequest = async () => {
    const result = await $http.post(createBrand, submitData)
    test2.innerHTML = `${JSON.stringify(result, null, 2)}`
};
postRequest();

// const patchRequest = async () => $http.edit(baz('some_id'), submitData);

// const deleteRequest = async () => $http.remove(baz('some_id') );
