const basePath: string = 'brand';

interface BrandEndpoints {
    getBrands: string
    createBrand: string
    removeBrand: (id: string) => string
}

const brandEndpoints: BrandEndpoints = {
    getBrands: `${basePath}`,
    createBrand: `${basePath}`,
    removeBrand: (id: string) => `${basePath}/${id}`,
}

export {brandEndpoints};
